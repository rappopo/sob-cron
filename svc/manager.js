const { _, parseDuration, DateTime, age, isSet, Sequelize } = require('rappopo-sob').Helper
const { CronJob } = require('cron')
const job = {}

const schema = {
  id: { type: Sequelize.STRING(50), allowNull: false, primaryKey: true },
  running: Sequelize.BOOLEAN,
  autoStart: Sequelize.BOOLEAN,
  runOnAutoStart: Sequelize.BOOLEAN,
  cronTime: Sequelize.STRING(50),
  lastRun: Sequelize.DATE,
  nextRun: Sequelize.DATE
}

const fields = _.concat(
  ['updatedAt'],
  _.keys(schema)
)

module.exports = ({ sob, sd }) => {
  sob.config.throttle = sob.config.throttle ? parseDuration(sob.config.throttle) : 0
  sob.config.delayOnStart = parseDuration(sob.config.delayOnStart)
  return {
    mixins: [sd],
    model: {
      define: schema,
      options: {
        createdAt: false,
        underscored: true
      }
    },
    settings: {
      fields,
      singleService: true,
      webApi: { disabled: ['create', 'remove'] }
    },
    run () {
      _.each(this.broker.services, s => {
        if (!s.settings.cron) return
        if (!_.isArray(s.settings.cron)) s.settings.cron = [s.settings.cron]
        _.each(s.settings.cron, (c, i) => {
          if (!isSet(c.start)) c.start = true
          c.timeout = c.timeout ? parseDuration(c.timeout) : 0
          let key = s.name
          if (c.name) key += _.upperFirst(_.camelCase(c.name))
          else if (_.isString(c.handler)) key += _.upperFirst(_.camelCase(c.handler))
          else key += (i + 1) + ''
          if (!job[key]) job[key] = { timeout: c.timeout, running: false, config: c, promise: null }
          const { timeZone, utcOffset, unrefTimeout } = c
          job[key].instance = new CronJob({
            cronTime: c.time,
            onTick: this.onTickWrapper(c, key, s),
            onComplete: this.onCompleteWrapper(c.onComplete, s),
            start: false,
            timeZone,
            runOnInit: false,
            utcOffset,
            unrefTimeout
          })
        })
      })
      this.logger.info(`${_.keys(job).length} job(s) initialized`)
      setTimeout(() => {
        _.forOwn(job, (v, k) => {
          if (v.config.autoStart) v.instance.start()
          if (v.config.runOnAutoStart) v.instance.fireOnTick()
        })
      }, sob.config.delayOnStart)
    },
    actions: {
      async list (ctx) {
        await this.loadRecords(ctx)
        const params = this.sanitizeParams(ctx, ctx.params)
        return this._list(ctx, params)
      },
      async get (ctx) {
        await this.loadRecords(ctx)
        const params = this.sanitizeParams(ctx, ctx.params)
        return this._get(ctx, params)
      },
      isJobRunning (ctx) {
        const j = job[ctx.params]
        return j ? !!j.running : undefined
      }
    },
    methods: {
      formatJob (name, rec) {
        return {
          id: name,
          running: !!rec.running,
          autoStart: rec.config.autoStart,
          runOnAutoStart: rec.config.runOnAutoStart,
          cronTime: rec.config.time,
          timeout: rec.config.timeout,
          lastRun: rec.instance.lastDate(),
          nextRun: rec.instance.nextDate()
        }
      },
      async loadRecords (ctx) {
        const values = []
        _.forOwn(job, (v, k) => {
          values.push(this.formatJob(k, v))
        })
        await this.actions.modelTruncate(ctx)
        await this.adapter.model.bulkCreate(values)
      },
      onTickWrapper (setting, cronName, svc) {
        const fn = () => {
          const j = job[cronName]
          if (j.running && j.timeout > 0) {
            const dur = Math.abs(j.running.diffNow().valueOf())
            const timeoutDt = j.running.plus(j.timeout)
            if (dur < j.timeout) {
              const text = `'${cronName}' still running since ${age(j.running)} ago, execution skipped. Timeout in: ${age(timeoutDt)}`
              this.logger.warn(text)
              return
            } else {
              this.logger.warn(`'${cronName}' timed out, execute new task forcefully`)
              if (j.promise.cancel) j.promise.cancel()
            }
          } else {
            this.logger.debug(`'${cronName}' starting...`)
          }
          j.running = DateTime.local()
          return new Promise((resolve, reject) => {
            Promise.resolve()
              .then(() => {
                let prom
                if (_.isFunction(setting.handler)) {
                  if (setting.handler.then) {
                    prom = setting.handler.apply(svc)
                  } else {
                    prom = new Promise((resolve, reject) => {
                      try {
                        resolve(setting.handler.apply(svc))
                      } catch (err) {
                        reject(err)
                      }
                    })
                  }
                  j.promise = prom
                  return prom
                }
                prom = this.broker.call(setting.handler.indexOf('.') > -1 ? setting.handler : (svc.name + '.' + setting.handler))
                j.promise = prom
                return prom
              })
              .then(result => {
                j.running = false
                j.promise = null
                if (result) this.logger.info(`'${cronName}' result: ${result}`)
                resolve(true)
              })
              .catch(err => {
                j.running = false
                j.promise = null
                this.logger.error(`'${cronName}' error: ${err.message}`)
                resolve(false)
              })
          })
        }
        return _.throttle(fn, sob.config.throttle)
      },
      onCompleteWrapper (name, svc) {
        if (!name) return null
        this.broker.call(name.indexOf('.') > -1 ? name : (svc.name + '.' + name))
      }
    }
  }
}
